//
//  ViewController.swift
//  SonarCloudSample3
//
//  Created by Keshav MB on 26/04/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.newMethod()
        self.workMethod()
        self.newMethod2()
    }

    func newMethod() {
        print("working code")
    }
    
    func workMethod(){
        print("Working method")
    }
    
    func newMethod2()
    {
        print("Woking new code2")
    }
    
}

